using CompanyExercise.Modelo;
using System.ComponentModel.Design;

namespace CompanyExercise
{
    public partial class Form1 : Form
    {
        private Architect architect;
        private Designer designer;
        private Programmer programmer;
        private Company company;
        public Form1()
        {
            InitializeComponent();

            architect = new Architect();
            designer = new Designer();
            programmer = new Programmer();
            company = new Company();
        }

        private void UpdateLabels()
        {
            LblDesigner.Text = designer.State ? "trabajando" : "no trabajando";
            LblProgrammer.Text = programmer.State ? "trabajando" : "no trabajando";
            LblArchitect.Text = architect.State ? "trabajando" : "no trabajando";
        }

        private void BtnProgrammer_Click(object sender, EventArgs e)
        {
            programmer.State = false;
            programmer.doWork();
            UpdateLabels();
        }

        private void BtnDesigner_Click(object sender, EventArgs e)
        {
            designer.State = false;
            designer.doWork();
            UpdateLabels();
        }

        private void BtnArchitect_Click(object sender, EventArgs e)
        {
            architect.State = false;
            architect.doWork();
            UpdateLabels();
        }

        private void BtnCreateSoftware_Click(object sender, EventArgs e)
        {
            company.CreateSoftware(architect, designer, programmer);
        }
    }
}
