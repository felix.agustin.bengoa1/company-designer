﻿namespace CompanyExercise
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            LblProgrammer = new Label();
            LblDesigner = new Label();
            LblArchitect = new Label();
            BtnDesigner = new Button();
            BtnProgrammer = new Button();
            BtnArchitect = new Button();
            LblCompany = new Label();
            BtnCreateSoftware = new Button();
            SuspendLayout();
            // 
            // LblProgrammer
            // 
            LblProgrammer.AutoSize = true;
            LblProgrammer.Location = new Point(22, 34);
            LblProgrammer.Name = "LblProgrammer";
            LblProgrammer.Size = new Size(97, 20);
            LblProgrammer.TabIndex = 1;
            LblProgrammer.Text = "Programador";
            // 
            // LblDesigner
            // 
            LblDesigner.AutoSize = true;
            LblDesigner.Location = new Point(22, 68);
            LblDesigner.Name = "LblDesigner";
            LblDesigner.Size = new Size(77, 20);
            LblDesigner.TabIndex = 2;
            LblDesigner.Text = "Diseñador";
            // 
            // LblArchitect
            // 
            LblArchitect.AutoSize = true;
            LblArchitect.Location = new Point(22, 102);
            LblArchitect.Name = "LblArchitect";
            LblArchitect.Size = new Size(79, 20);
            LblArchitect.TabIndex = 3;
            LblArchitect.Text = "Arquitecto";
            // 
            // BtnDesigner
            // 
            BtnDesigner.Location = new Point(125, 65);
            BtnDesigner.Name = "BtnDesigner";
            BtnDesigner.Size = new Size(70, 29);
            BtnDesigner.TabIndex = 4;
            BtnDesigner.Text = "Work";
            BtnDesigner.UseVisualStyleBackColor = true;
            BtnDesigner.Click += BtnDesigner_Click;
            // 
            // BtnProgrammer
            // 
            BtnProgrammer.Location = new Point(125, 30);
            BtnProgrammer.Name = "BtnProgrammer";
            BtnProgrammer.Size = new Size(70, 29);
            BtnProgrammer.TabIndex = 5;
            BtnProgrammer.Text = "Work";
            BtnProgrammer.UseVisualStyleBackColor = true;
            BtnProgrammer.Click += BtnProgrammer_Click;
            // 
            // BtnArchitect
            // 
            BtnArchitect.Location = new Point(125, 100);
            BtnArchitect.Name = "BtnArchitect";
            BtnArchitect.Size = new Size(70, 29);
            BtnArchitect.TabIndex = 6;
            BtnArchitect.Text = "Work";
            BtnArchitect.UseVisualStyleBackColor = true;
            BtnArchitect.Click += BtnArchitect_Click;
            // 
            // LblCompany
            // 
            LblCompany.AutoSize = true;
            LblCompany.Location = new Point(22, 166);
            LblCompany.Name = "LblCompany";
            LblCompany.Size = new Size(72, 20);
            LblCompany.TabIndex = 7;
            LblCompany.Text = "Company";
            // 
            // BtnCreateSoftware
            // 
            BtnCreateSoftware.Location = new Point(125, 166);
            BtnCreateSoftware.Name = "BtnCreateSoftware";
            BtnCreateSoftware.Size = new Size(70, 29);
            BtnCreateSoftware.TabIndex = 8;
            BtnCreateSoftware.Text = "Work";
            BtnCreateSoftware.UseVisualStyleBackColor = true;
            BtnCreateSoftware.Click += BtnCreateSoftware_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(223, 240);
            Controls.Add(BtnCreateSoftware);
            Controls.Add(LblCompany);
            Controls.Add(BtnArchitect);
            Controls.Add(BtnProgrammer);
            Controls.Add(BtnDesigner);
            Controls.Add(LblArchitect);
            Controls.Add(LblDesigner);
            Controls.Add(LblProgrammer);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private Label LblProgrammer;
        private Label LblDesigner;
        private Label LblArchitect;
        private Button BtnDesigner;
        private Button BtnProgrammer;
        private Button BtnArchitect;
        private Label LblCompany;
        private Button BtnCreateSoftware;
    }
}
