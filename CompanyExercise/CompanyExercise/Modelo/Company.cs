﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyExercise.Modelo
{
    public class Company
    {
        public void CreateSoftware(Architect architect, Designer designer, Programmer programmer)
        {
            
            if(architect.State && designer.State && programmer.State)
            {
                MessageBox.Show("todos los empleados estan trabajando");

                designer.State = false;
                architect.State = false;
                programmer.State = false;
            }
            else
            {
                string errorMessage = "algunos de los empleados no estan trabajando:\n";
                if (!architect.State)
                    errorMessage += "El arquitecto no está trabajando.\n";
                if (!designer.State)
                    errorMessage += "El diseñador no está trabajando.\n";
                if (!programmer.State)
                    errorMessage += "El programador no está trabajando.\n";

                MessageBox.Show(errorMessage);

                designer.State = false;
                architect.State = false;
                programmer.State = false;
            }
            
        }
    }
}
