﻿using CompanyExercise.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyExercise.Modelo
{
    public class Architect : IEmployee
    {
        public bool State { set; get; } = false;

        public void doWork()
        {
            State = true;
        }
    }
}
